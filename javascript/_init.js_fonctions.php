<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function charge_init_scripts(string $prive = '') {

	$dir = ($prive ? 'prive/' : '') . _JAVASCRIPT . '_inits/';
	// pour la partie publique, a priori,
	// on ne veut pas ce qui provient d'un dossier prive/
	$pattern = ($prive ? '.js(.html)?$' : '^(?!.*prive/).*js(.html)?$');

	$files = find_all_in_path($dir, $pattern);

	$liste_normalisee = [];

	foreach (array_keys($files) as $k) {
		// on remplit un tableau avec une version normalisee du nom
		// (sans index ni extension .html) qui servira à évacuer les doublons
		$cle_normalisee = preg_replace('/^[0-9]*_/', '', $k);
		$cle_normalisee = preg_replace('/.html$/', '', $cle_normalisee);

		$liste_normalisee[$k] = $cle_normalisee;

		// pour la partie publique, si un fichier trouvé est vide mais homonyme
		// d'un fichier accessible depuis prive/, alors on renvoie ce dernier
		// dans la liste originelle, ce qui permet une surcharge "souple"
		if (
			!$prive
			&& (!filesize($files[$k]))
			&& ($fallback_prive = find_all_in_path('prive/' . $dir, $cle_normalisee))
			&& !empty($fallback_prive)
		) {
			$files[$k] = reset($fallback_prive);
		}

		// nettoyer (realpath)
		$files[$k] = substr($files[$k], strlen(_DIR_RACINE));
	}

	$liste_dedoublonnee = array_unique($liste_normalisee) ?? [];

	ksort($files); // appliquer le tri des index

	/*
		var_dump( [
			//'chemin' => _chemin(),
			'files_raw' => $files,
			'normalisee' => $liste_normalisee,
			'liste_dedoublonnee' => $liste_dedoublonnee,
			'files' => array_intersect_key($files, $liste_dedoublonnee)
		]);
	*/

	return array_intersect_key($files, $liste_dedoublonnee);
}
