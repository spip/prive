import {ajaxbloc} from 'ajaxCallback.js';
function cibler_ajaxbloc(_root) {
	let root = _root || document;
	if (root !== document && root.classList.contains("ajaxbloc")) {
		root = root.parentNode;
	}
	ajaxbloc(root);
}
addCSS(spip.css.ajax,'ajax');
cibler_ajaxbloc();
onAjaxLoad(cibler_ajaxbloc);