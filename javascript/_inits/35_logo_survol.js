import {logo_survol} from "presentation.js";

for (const sel of [...document.querySelectorAll('.spip_logo_survol')]) {
	logo_survol(sel);
}
onAjaxLoad((_root) => {
	const root = _root || document;
	for (const sel of [...root.querySelectorAll('.spip_logo_survol')]
		.filter( e => !e.matches(['data-src-original']))
	) {
		logo_survol(sel);
	}
});
