import { GlobalRegistrator } from "@happy-dom/global-registrator";
import { mock } from "bun:test";

mock.module("config.js", () => ({
	spip: {
		load_handlers: [],
	},
}));

GlobalRegistrator.register();
