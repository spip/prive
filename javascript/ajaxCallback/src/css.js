/**
 * Écrire une balise <style> dans <head> avec du contenu
 *
 * @param {String} payload  contenu brut CSS à écrire
 * @param {String} identifiant sert à personnaliser un [id] sur la balise style
 * @param {Boolean} (false) si true, écrase la balise déjà existante
 */
export function addCSS(payload, identifiant = "", force = false) {
	const id = identifiant ? `dyn_css_${identifiant}` : "dyn_css";
	const old = document.getElementById(id);
	if (payload && document.head && (!old || force)) {
		if (force) {
			old.parentNode.removeChild(old);
		}
		const style = document.createElement("style");
		style.setAttribute("id", id);
		style.innerHTML = payload;
		document.head.appendChild(style);
	}
}
