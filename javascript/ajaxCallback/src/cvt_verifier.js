import { onAjaxLoad } from "./ajaxbloc.js";
import { log } from "./log.js";
import { formulaire_set_container } from "./ajaxform.js";

/**
 * Poster un formulaire ajaxé pour obtenir en format JSON
 * le tableau d'erreurs issu de la partie `verifier` de CVT
 *
 * @param  {HTMLElement} formulaire déjà ajaxé
 * @param  {Function} callback de traitement des données reçues
 * @param  {HTMLElement} champ du formulaire impliqué dans l'action en cours
 */
export function formulaire_verifier(form, callback, champ) {
	// si on est aussi en train de submit pour de vrai, abandonner
	if (form.closest(".ajax-form-container").dataset.ariaBusy !== "true") {
		if (form.classList.contains("hasajax")) {
			const params = new URLSearchParams([...new FormData(form).entries()]);
			params.append("formulaire_action_verifier_json", true);
			fetch(form.action, {
				method: "POST",
				body: params,
			})
				.then((response) => {
					if (!response.ok) {
						throw new Error(
							`[formulaire_verifier] ${form.action} ${response.status} ${response.statusText}`,
						);
					}
					return response.json();
				})
				.then((data) => {
					if (form.closest(".ajax-form-container").dataset.ariaBusy !== "true") {
						callback.apply(null, [form, data, champ]);
					}
				});
		} else {
			callback.apply(null, [form, { message_erreur: "form non ajax" }, champ]);
		}
	}
}


/**
 * Activer la vérification automatique des erreurs lors
 * d'un changement sur les champs d'un formulaire CVT ajaxé
 *
 * @param  {HTMLElement} target  formulaire cible
 * @param  {Function} callback de traitement des données
 */
export function formulaire_activer_verif_auto(target, callback) {
	callback = callback || formulaire_actualiser_erreurs;
	formulaire_set_container(target);
	const form = target.matches("form") ? target : target.querySelector("form");

	const activer = () => {
		if (form.getAttribute("data-verifjson") !== "on") {
			form.dataset.verifjson = "on";
			form.addEventListener("change", (e) => {
				if (e.target.matches('input:not([type="hidden"]),select,textarea')) {
					// declencher apres 50ms pour ne pas double submit sur sequence saisie+submit
					setTimeout(() => {
						formulaire_verifier(form, callback, e.target.getAttribute("name"));
					}, 50);
				}
			});
		}
	};
	activer();
	onAjaxLoad(() => {
		setTimeout(activer, 150);
	});
}

/**
 * Fonction d'affichage/mise à jour des erreurs d'un formulaire CVT
 *
 * @param  {HTMLElement} target  formulaire cible
 * @param  {Object} erreurs  JSON {champ_1:message_erreur_1,...}
 */
export function formulaire_actualiser_erreurs(target, erreurs) {
	log(["formulaire_actualiser_erreurs", target, erreurs]);

	const formParent = target.classList.contains("formulaire_spip")
		? target
		: target.closest(".formulaire_spip");
	if (!formParent) return;
	// d'abord effacer tous les messages d'erreurs
	for (const reponse of [
		...formParent.querySelectorAll(".reponse_formulaire,.erreur_message"),
	]) {
		reponse.parentNode.removeChild(reponse);
		//reponse.animateRemove();
	}
	for (const err of [...formParent.querySelectorAll(".erreur")]) {
		err.classList.remove("erreur");
	}
	// ensuite afficher les nouveaux messages d'erreur
	if (erreurs.message_ok) {
		const _ok = document.createElement("div");
		_ok.setAttribute("class", "reponse_formulaire reponse_formulaire_ok");
		_ok.innerHTML = erreurs.message_ok;
		formParent.insertBefore(_ok, formParent.firstChild);
	}
	if (erreurs.message_erreur) {
		const _err = document.createElement("div");
		_err.setAttribute("class", "reponse_formulaire reponse_formulaire_erreur");
		_err.innerHTML = erreurs.message_erreur;
		formParent.insertBefore(_err, formParent.firstChild);
	}
	for (const k in erreurs) {
		const saisie = formParent.querySelector(`.editer_${k}`);
		if (saisie) {
			saisie.classList.add("erreur");
			const _err_saisie = document.createElement("span");
			_err_saisie.setAttribute("class", "erreur_message");
			_err_saisie.innerHTML = erreurs[k];
			saisie
				.querySelector("label")
				.parentNode.insertBefore(
					_err_saisie,
					saisie.querySelector("label").nextElementSibling,
				);
		}
	}
}
