/**
 * Masque un élément avec un effet de repli
 * @param {HTMLElement} element
 * @param {Number} duration
 * @returns {Promise<boolean>}
 */
export async function slideUp(element, duration = 500) {
	return new Promise((resolve, reject) => {
		element.style.height = `${element.offsetHeight}px`;
		element.style.transitionProperty = "height, margin, padding";
		element.style.transitionDuration = `${duration}ms`;
		element.offsetHeight;
		element.style.overflow = "hidden";
		element.style.height = 0;
		element.style.paddingTop = 0;
		element.style.paddingBottom = 0;
		element.style.marginTop = 0;
		element.style.marginBottom = 0;
		window.setTimeout(() => {
			element.style.display = "none";
			element.style.removeProperty("height");
			element.style.removeProperty("padding-top");
			element.style.removeProperty("padding-bottom");
			element.style.removeProperty("margin-top");
			element.style.removeProperty("margin-bottom");
			element.style.removeProperty("overflow");
			element.style.removeProperty("transition-duration");
			element.style.removeProperty("transition-property");
			resolve(false); // pour un usage pertinent avec classList.toggle
		}, duration);
	});
}
// Appel possible sur HTMLElement
HTMLElement.prototype.slideUp = async function (duration) {
	await slideUp(this, duration);
};
/**
 * Affiche un élément avec un effet de dépliement
 * @param {HTMLElement} element
 * @param {Number} duration
 * @returns {Promise<boolean>}
 */
export async function slideDown(element, duration = 500) {
	return new Promise((resolve, reject) => {
		element.style.removeProperty("display");
		let display = window.getComputedStyle(element).display;
		if (display === "none") display = "block";
		element.style.display = display;
		const height = element.offsetHeight;
		element.style.overflow = "hidden";
		element.style.height = 0;
		element.style.paddingTop = 0;
		element.style.paddingBottom = 0;
		element.style.marginTop = 0;
		element.style.marginBottom = 0;
		element.offsetHeight; // eslint-disable-line no-unused-expressions
		element.style.transitionProperty = "height, margin, padding";
		element.style.transitionDuration = `${duration}ms`;
		element.style.height = `${height}px`;
		element.style.removeProperty("padding-top");
		element.style.removeProperty("padding-bottom");
		element.style.removeProperty("margin-top");
		element.style.removeProperty("margin-bottom");
		window.setTimeout(() => {
			element.style.removeProperty("height");
			element.style.removeProperty("overflow");
			element.style.removeProperty("transition-duration");
			element.style.removeProperty("transition-property");
			resolve(true);
		}, duration);
	});
}
// Appel possible sur HTMLElement
HTMLElement.prototype.slideDown = async function (duration) {
	await slideDown(this, duration);
};

/**
 * Affiche ou Masque un élément avec un effet de repli
 * @param {HTMLElement} element
 * @param {Number} duration
 * @returns {Promise<boolean>}
 */
export async function slideToggle(element, duration = 500) {
	if (window.getComputedStyle(element).display === "none") {
		return await slideDown(element, duration);
	} else {
		return await slideUp(element, duration);
	}
}
// Appel possible sur HTMLElement
HTMLElement.prototype.slideToggle = async function (duration) {
	await slideToggle(this, duration);
};
