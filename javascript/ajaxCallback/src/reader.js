/**
 * Deux fonctions pour rendre l'ajax compatible Jaws
 */
import { addCSS } from "./css.js";

const id = "virtualbufferupdate";

export function initReaderBuffer() {

	if (document.getElementById(id)) {
		return;
	}
	addCSS(
		`#${id} {position:absolute;left:-9999em;}`,
		"virtualbuffer",
	);

	const input = document.createElement("input");
	input.setAttribute("id", id);
	input.setAttribute("type", "hidden");
	input.setAttribute("value", 0);
	document.body.appendChild(input);
}

export function updateReaderBuffer() {
	const buffer = document.getElementById(id);
	if (buffer) {
		// incrementons l'input hidden, ce qui a pour effet de forcer le rafraichissement du
		// buffer du lecteur d'ecran (au moins dans Jaws)
		buffer.value++;
	}
}
