/**
 * Equivalent js de parametre_url php de spip
 *
 * Exemples :
 * parametre_url(url,suite,18) (ajout)
 * parametre_url(url,suite,'') (supprime)
 * parametre_url(url,suite) (lit la valeur suite)
 * parametre_url(url,suite[],1) (tableau valeurs multiples)
 * @param url
 *   url
 * @param c
 *   champ
 * @param v
 *   valeur
 * @param sep
 *  separateur '&' par defaut
 * @param force_vide
 *  si true et v='' insere &k= dans l'url au lieu de supprimer le k (false par defaut)
 *  permet de vider une valeur dans une requete ajax (dans un reload)
 */
export function parametre_url(url, c, v, sep, force_vide) {
	// Si l'URL n'est pas une chaine, on ne peut pas travailler dessus et on quitte
	if (typeof url == "undefined") {
		url = "";
	}

	var p;
	// lever l'#ancre
	var ancre = "";
	var a = "./";
	var args = [];
	p = url.indexOf("#");
	if (p != -1) {
		ancre = url.substring(p);
		url = url.substring(0, p);
	}

	// eclater
	p = url.indexOf("?");
	if (p !== -1) {
		// recuperer la base
		if (p > 0) a = url.substring(0, p);
		args = url.substring(p + 1).split("&");
	} else a = url;
	var regexp = new RegExp(
		"^(" + c.replace("[]", "\\[\\]") + "\\[?\\]?)(=.*)?$",
	);
	var ajouts = [];
	var u = typeof v !== "object" ? encodeURIComponent(v) : v;
	var na = [];
	var v_read = null;
	// lire les variables et agir
	for (var n = 0; n < args.length; n++) {
		var val = args[n];
		try {
			val = decodeURIComponent(val);
		} catch (e) {}
		var r = val.match(regexp);
		if (r && r.length) {
			if (v == null) {
				// c'est un tableau, on memorise les valeurs
				if (r[1].substr(-2) == "[]") {
					if (!v_read) v_read = [];
					v_read.push(
						r.length > 2 && typeof r[2] !== "undefined"
							? r[2].substring(1)
							: "",
					);
				}
				// c'est un scalaire, on retourne direct
				else {
					return r.length > 2 && typeof r[2] !== "undefined"
						? r[2].substring(1)
						: "";
				}
			}
			// suppression
			else if (!v.length) {
			}
			// Ajout. Pour une variable, remplacer au meme endroit,
			// pour un tableau ce sera fait dans la prochaine boucle
			else if (r[1].substr(-2) != "[]") {
				na.push(r[1] + "=" + u);
				ajouts.push(r[1]);
			}
			/* Pour les tableaux ont laisse tomber les valeurs de départ, on
			remplira à l'étape suivante */
			// else na.push(args[n]);
		} else na.push(args[n]);
	}

	if (v == null) return v_read; // rien de trouve ou un tableau
	// traiter les parametres pas encore trouves
	if (v || v.length || typeof(v) === "number" || force_vide) {
		ajouts = "=" + ajouts.join("=") + "=";
		var all = c.split("|");
		for (n = 0; n < all.length; n++) {
			if (ajouts.search("=" + all[n] + "=") == -1) {
				if (typeof v !== "object") {
					na.push(all[n] + "=" + u);
				} else {
					var id = all[n].substring(-2) == "[]" ? all[n] : all[n] + "[]";
					for (p = 0; p < v.length; p++)
						na.push(id + "=" + encodeURIComponent(v[p]));
				}
			}
		}
	}

	// recomposer l'adresse
	if (na.length) {
		if (!sep) sep = "&";
		a = a + "?" + na.join(sep);
	}

	return a + ancre;
}
