import { test, expect } from "bun:test";
import { formulaire_fix_markup_post_submit } from '../src/ajaxform.js';

test("formulaire_fix_markup_post_submit", () => {
  document.body.innerHTML = `
<div class="ajax-form-container" aria-busy="false">
	<div class="ajax">
		<div class="formulaire_spip formulaire_configurer formulaire_configurer-new">
			<br class='bugajaxie ajax-form-is-ok' style='display:none;'>
			<div class="formulaire_spip formulaire_configurer formulaire_configurer-new --scroll">
				<h3 class='titrem'><img src="../prive/themes/spip/images/identite-xx.svg" class="cadre-icone">Identité du site</h3>
				<div class="reponse_formulaire reponse_formulaire_ok" role="status">La nouvelle configuration a été enregistrée</div>
				<form method='post' action='/ecrire/?exec=configurer_identite'></form>
			</div>
		</div>
	</div>
</div>
`;
  const markup_post_submit = document.querySelector(".ajax-form-container");
  formulaire_fix_markup_post_submit(markup_post_submit);
  const br = document.querySelector(".bugajaxie");
  expect(br).toBe(null);
  expect(markup_post_submit.outerHTML).toMatchSnapshot();
});
