import { test, expect } from "bun:test";
import { formulaire_switch_ajax } from "../src/ajaxform.js";

test("formulaire_switch_ajax", () => {
	document.body.innerHTML = `
<div class="formulaire_spip formulaire_test">
	<form action="./" method="post">
		<input type="hidden" name="var_foo" value="test"/>
		<p class="boutons">
			<input type="submit" class="submit" value="<:pass_ok:>" />
		</p>
	</form>
</div>
`;
	const form_test = document.querySelector("form");
	function check_input_ajax() {
		return [...form_test.querySelectorAll('input[name="var_ajax"]')].length;
	}
	expect(check_input_ajax()).toBe(0);
	// activer l'ajax plusieurs fois n'ajoute jamais plus d'un input
	formulaire_switch_ajax(form_test);
	formulaire_switch_ajax(form_test, true);
	formulaire_switch_ajax(form_test, true);
	expect(check_input_ajax()).toBe(1);

	formulaire_switch_ajax(form_test, false);
	expect(check_input_ajax()).toBe(0);
});
