export const login_info = {};
const input_login = document.getElementById("var_login");
const input_session = document.getElementById("session_remember");
const logo = document.getElementById("spip_logo_auteur");

// payload : JSON envoye par informer_auteur.html
function informe_auteur(payload) {
	if (typeof payload === "object") {
		// indiquer le cnx si on n'y a pas touche
		if (!input_session.classList.contains("modifie")) {
			input_session.checked = !!payload.cnx;
		}
		logo.innerHTML = payload.logo ?? "";
	}
}

function actualise_auteur() {
	if (
		(input_login?.value?.length ?? 0) > 2 &&
		!login_info.informe_auteur_en_cours &&
		login_info.login !== input_login.value
	) {
		login_info.informe_auteur_en_cours = true;
		login_info.login = input_login.value;
		const data = new FormData();
		data.append("var_login", login_info.login);
		fetch(login_info.page_auteur, {
			method: "POST",
			body: data,
		})
			.then((response) => {
				return response.json();
			})
			.then((data) => {
				login_info.informe_auteur_en_cours = false;
				informe_auteur(data);
			});
	}
}

export function surveille_login() {
	for (const ev of ["change", "input"]) {
		input_login.addEventListener(
			ev,
			() => {
				actualise_auteur();
			},
			// lors du premier input qui peut correspondre à
			// la validation autocomplete proposée par le navigateur
			{ once: ev === "input" },
		);
	}
}
