import { throttle } from "ajaxCallback.js";

/**
 * Ajouter une classe à un élément lors du survol
 * @uses throttle()
 * @param  {HTMLElement} target
 * @param  {string} toggleClass
 */
export function hoverClass(target, _toggleClass) {
	const toggleClass = _toggleClass || "on";
	target.addEventListener(
		"mouseover",
		throttle(() => {
			target.classList.add(toggleClass);
		}, 100),
	);
	target.addEventListener(
		"mouseout",
		throttle(() => {
			target.classList.remove(toggleClass);
		}, 100),
	);
}
// Appel possible sur HTMLElement
HTMLElement.prototype.hoverClass = function (toggleClass) {
	return hoverClass(this, toggleClass);
};
// Appel possible sur sur une NodeList
NodeList.prototype.hoverClass = function (toggleClass) {
	this.forEach((el) => {
		el.hoverClass(toggleClass);
	});
};
