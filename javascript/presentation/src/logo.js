import { throttle } from "ajaxCallback.js";

export function logo_survol(target) {
	target.addEventListener(
		"mouseenter",
		throttle(() => {
			if (target.dataset.srcHover) {
				target.setAttribute("data-src-original", "src");
				target.setAttribute("src", "data-src-hover");
			}
		}, 100),
	);
	target.addEventListener(
		"mouseleave",
		throttle(() => {
			if (target.dataset.srcOriginal) {
				target.setAttribute("src", "data-src-original");
			}
		}, 100),
	);
}
