import { spip } from "config.js";
import { parametre_url } from "ajaxCallback.js";
import { hoverClass } from "./hoverClass.js";

/**
 * Utilisee dans inc/puce_statut pour les puces au survol
 * @param int id
 * @param strong type
 * @param int decal
 * @param string puce
 * @param string script
 */
function selec_statut(id, type, decal, puce, script) {

	const node = document.querySelector(`.imgstatut${type}${id}`);
	const accepter_change_statut = confirm(spip.alerts.confirm_changer_statut);

	if (!accepter_change_statut || !node) {
		return;
	}

	const statutdecal = document.querySelector(`.statutdecal${type}${id}`);
	const w =
		window.getComputedStyle(statutdecal).getPropertyValue("width") || "auto";
	statutdecal.style = `margin-left:${decal};width:${w};`;
	statutdecal.classList.remove("on");

	fetch(script)
		.then((response) => {
			return response.text();
		})
		.then((data) => {
			if (!data) {
				node.setAttribute("src", puce);
			} else {
				const r = window.open();
				r.document.write(data);
				r.document.close();
			}
		});
}

/**
 * Utilisee dans inc/puce_statut pour les puces au survol
 * @param objet node
 * @param string nom
 * @param string type
 * @param int id
 * @param string action
 */
function prepare_selec_statut(node, nom, type, id, action) {
	hoverClass(node);
	action = parametre_url(action, "type", type);
	action = parametre_url(action, "id", id);
	fetch(action, { method: "GET" })
		.then((response) => {
			return response.text();
		})
		.then((data) => {
			node.innerHTML = data;
			puce_survol(node);
		});
}

export function puce_survol(_root) {
	const root = _root || document;
	for (const popup of [
		...root.querySelectorAll("span.puce_objet_popup a"),
	].filter((e) => !e.classList.contains("puce-survol-enabled"))) {
		popup.addEventListener("click", (e) => {
			e.preventDefault();
			e.stopPropagation();
			selec_statut(
				popup.getAttribute("data-puce-id"),
				popup.getAttribute("data-puce-type"),
				popup.getAttribute("data-puce-decal"),
				popup.firstChild.src,
				popup.getAttribute("data-puce-action"),
			);
		});
		popup.classList.add("puce-survol-enabled");
	}
	for (const hover of [...root.querySelectorAll("span.puce_objet")].filter(
		(e) => !e.classList.contains("puce-survol-enabled"),
	)) {
		hover.addEventListener("mouseover", (e) => {
			e.stopPropagation();
			if (hover.getAttribute("data-puce-loaded") === null) {
				prepare_selec_statut(
					hover,
					hover.getAttribute("data-puce-nom"),
					hover.getAttribute("data-puce-type"),
					hover.getAttribute("data-puce-id"),
					hover.getAttribute("data-puce-action"),
				);
				hover.setAttribute("data-puce-loaded", "");
			}
		});
		hover.classList.add("puce-survol-enabled");
	}
}
