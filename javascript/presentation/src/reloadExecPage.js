import { ajaxReload } from "ajaxCallback.js";

export function reloadExecPage(exec, _blocs) {
	const blocs = _blocs || "#navigation,#extra";
	for (const bloc of document.querySelectorAll(blocs)) {
		const ajaxbloc = bloc.firstElementChild;
		if (ajaxbloc) {
			ajaxReload(bloc.firstElementChild, { args: { exec } });
		}
	}
	document.body.classList.toggle("edition", exec.match(/_edit$/));
}
