import {
	ajaxClick,
	ajaxReload,
	ajaxbloc,
	animateAppend,
	animateLoading,
	animateRemove,
	endLoading,
	followLink,
	formulaire_actualiser_erreurs,
	formulaire_dyn_ajax,
	formulaire_set_container,
	formulaire_verifier,
	log,
	onAjaxLoad,
	parametre_url,
	positionner,
	triggerAjaxLoad,
} from "ajaxCallback.js";
/**
 * COMPATIBILITE :
 * - mapper les variables/fonctions/propriétés legacy de l'objet window vers le nouvel objet `config.js`
 * - mapper les fonctions legacy de l'Objet jQuery `$.fn.xxx` vers leur équivalent plainjs
 * - étendre HTMLElement.prototype et NodeList.prototype pour tâcher de supporter la syntaxe à la chaîne façon JQuery
 * - tracer certains usages dépreciés dans la console (voir ?var_mode=debug_js)
 */
import { spip } from "config.js";

const to_global = {
	ajaxClick,
	ajaxReload,
	animateAppend,
	animateLoading,
	animateRemove,
	endLoading,
	onAjaxLoad,
	followLink,
	positionner,
	parametre_url,
	triggerAjaxLoad,
};
// tracer l'appel aux fonctions historiques qui n'utilisent pas `import`
for (const k of Object.keys(to_global)) {
	if (!Object.hasOwn(window, k)) {
		Object.defineProperty(window, k, {
			get: () => {
				trace_variable_migree(k);
				return to_global[k];
			},
		});
	}
}

const variables_perdues = [
	"ajax_image_searching",
	"stat",
	"confirm_changer_statut",
	"url_menu_rubrique",
	"ajaxbloc_selecteur",
	"var_zajax_content",
];
// chercher les appels historiques à window.XXX dans l'objet de config
for (const v of variables_perdues) {
	if (!Object.hasOwn(window, v)) {
		Object.defineProperty(window, v, {
			get: () => {
				trace_variable_migree(v);
				return find_in_config(spip, v);
			},
		});
	}
}

function trace_old_compat() {
	if (spip.debug) {
		console.trace(
			"Vous utilisez une fonction obsolète, liée à jQuery. Pensez à migrer si possible",
		);
	}
}

function trace_variable_migree(quoi) {
	if (spip.debug) {
		console.trace(
			`Accéder à ${quoi} depuis l'objet window est déconseillé. Privilégiez le recours à import`,
		);
	}
}

function find_in_config(obj, query) {
	for (const key in obj) {
		const value = obj[key];
		if (typeof value === "object") {
			find_in_config(value, query);
		}
		if (key === query) {
			return value;
		}
	}
}

// Appels possibles sur HTMLElement
HTMLElement.prototype.ajaxbloc = function (options) {
	ajaxbloc(this, options);
};
HTMLElement.prototype.ajaxReload = function (options) {
	ajaxReload(this, options);
};
HTMLElement.prototype.followLink = function () {
	followLink(this);
};
HTMLElement.prototype.formulaire_dyn_ajax = function () {
	formulaire_dyn_ajax(this);
};
HTMLElement.prototype.formulaire_set_container = function () {
	formulaire_set_container(this);
};
HTMLElement.prototype.positionner = function (force, setfocus) {
	positionner(this, force, setfocus);
};
HTMLElement.prototype.animateLoading = function () {
	animateLoading(this);
};
HTMLElement.prototype.endLoading = function (hard) {
	endLoading(this, hard);
};
HTMLElement.prototype.animateRemove = function (callback) {
	animateRemove(this, callback);
};
HTMLElement.prototype.animateAppend = function (callback) {
	animateAppend(this, callback);
};
HTMLElement.prototype.formulaire_verifier = function (callback, champ) {
	formulaire_verifier(this, callback, champ);
};
HTMLElement.prototype.formulaire_actualiser_erreurs = function (erreurs) {
	formulaire_actualiser_erreurs(this, erreurs);
};

// Appels possibles sur sur une NodeList
NodeList.prototype.ajaxbloc = function (options) {
	this.forEach((el) => {
		ajaxbloc(el, options);
	});
};
NodeList.prototype.ajaxReload = function (options) {
	this.forEach((el) => {
		ajaxReload(el, options);
	});
};

document.addEventListener("DOMContentLoaded", () => {
	if (typeof jQuery === "function") {
		jQuery.fn.extend({
			ajaxReload: function (options) {
				trace_old_compat();
				return this.each(function () {
					ajaxReload(this, options);
				});
			},
			animateAppend: function (callback) {
				trace_old_compat();
				return this.each(function () {
					animateAppend(this, callback);
				});
			},
			animateLoading: function () {
				trace_old_compat();
				return this.each(function () {
					animateLoading(this);
				});
			},
			animeajax: function () {
				trace_old_compat();
				return this.each(function () {
					animateLoading(this);
				});
			},
			endLoading: function (arg) {
				trace_old_compat();
				return this.each(function () {
					endLoading(this, arg);
				});
			},
			animateRemove: function (callback) {
				trace_old_compat();
				return this.each(function () {
					animateRemove(this, callback);
				});
			},
			positionner: function (force, setfocus) {
				trace_old_compat();
				return this.each(function () {
					positionner(this, force, setfocus);
				});
			},
			followLink: function () {
				trace_old_compat();
				return followLink(this[0]);
			},
		});
	}
});
