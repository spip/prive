<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function importmap_json_encode($arr) {
	return json_encode($arr, JSON_PRETTY_PRINT);
}

function importmap_basename($cle) {
	return basename($cle, '.html');
}
